import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import Tweet from './Tweet';
import { Container, Row, Col } from 'reactstrap';

function App() {
  return (
    <section className="bg-light">
      <Container className="pt-5 pb-5">
        <Row className="justify-content-center">
          <Col md="8">
            <Tweet />
          </Col>
        </Row>

      </Container>
    </section>
  );
}

export default App;
