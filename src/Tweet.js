import React from 'react';
import { Row, Col, Card, CardBody } from 'reactstrap';

class Tweet extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      items: []
    };
  }

  componentDidMount() {
    fetch("/api/1.1/statuses/user_timeline.json?screen_name=Satyam_yadav&count=50")
      .then(res => res.json())
      .then(
        (result) => {
          console.log(result)
          this.setState({
            isLoaded: true,
            items: result
          });
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        (error) => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      )
  }

  render() {
    const { error, isLoaded, items } = this.state;
    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
      return <div>Loading...</div>;
    } else {
      return (
        <>
          {items.map(({ user, ...item }) => {
            if (!!item.retweeted_status) {
              user = { ...item.retweeted_status.user }
            }
            return (
              <Card className="border-bottom-0 rounded-0" key={item.id}>
                <CardBody>
                  <Row>
                    <Col sm="1" className="p-1">
                      <img className="rounded-circle w-100 img-fluid" src={user.profile_image_url} />
                    </Col>
                    <Col >
                      <h5>
                        {user.name}
                        <small className="ml-2">
                          {item.created_at}
                        </small>
                      </h5>
                      <h6>
                        {item.text}
                      </h6>
                      <div>

                        {
                          item.extended_entities && (
                            item.extended_entities.media.map(({ media_url = '' }) => {
                              return (<img className="img-fluid rounded" src={media_url} />)

                            })
                          )
                        }
                      </div>
                    </Col>
                  </Row>

                </CardBody>
              </Card>
            )
          })}
        </>
      );
    }
  }
}

export default Tweet;
